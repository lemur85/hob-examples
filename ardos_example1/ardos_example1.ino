#include <kernel.h>
#include <queue.h>
#include <sema.h>
#include <mutex.h>
#include <Servo.h>

/* Number of OS tasks */
#define NUM_TASKS  3

/* I/O definitions */
#define LED1   2
#define LED2   3
#define LED3   4
#define SERVO1 9
#define SERVO2 10


/* LED delay between flashes in ms */
static uint16_t led_delay = 250;

/* Servo delay between each movement in ms */
static uint16_t servo_delay = 3;

/* Servos controllers declaration */
static Servo servo1;
static Servo servo2;

/* Tasks definitions */
void leds_flashing(void* data);
void move_servo1(void* data);
void move_servo2(void* data);



/* Setup function */
void setup(void) {
  
  /* Declare ArdOS number of tasks */
  OSInit(NUM_TASKS);

  /* Configure output I/O and servos */
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  
  servo1.attach(SERVO1);
  servo2.attach(SERVO2);
  
  /* Create ArdOS tasks */
  OSCreateTask(0, leds_flashing, NULL);
  OSCreateTask(1, move_servo1, NULL);
  OSCreateTask(2, move_servo2, NULL);
  
  /* Run ArdOS*/
  OSRun();
}

/* Loop function, not used in ArdOS */
void loop(void) {
  /* Nothing to do */
}

/************** TASKS CODE *******************/

/**
 * First task for led flashing. Leds are shifted and 
 * flashed each 'led_delay' ms.
 */
void leds_flashing(void* data) {
  uint8_t led_id = LED1;
  while(1) {
    digitalWrite(led_id, HIGH);
    
    OSSleep(led_delay);
    
    digitalWrite(led_id, LOW);
    
    led_id = (led_id == LED3) ? LED1 : led_id+1;
  }
}

/*
 * First servo movement. From 0 to 160, an then back from 160 to 0
 */
void move_servo1(void* data) {
  uint8_t i=0;
  while(1) {

    for(i=0;i<160;i++) {
      servo1.write(i);
      
      OSSleep(servo_delay);
    }
    
    for(i=160;i>0;i--) {
      servo1.write(i);
      
      OSSleep(servo_delay);
    }
  } 
}


/*
 * Second servo movement. From 160 to 0, an then back from 0 to 160
 */
void move_servo2(void* data) {
  uint8_t i=0;
  while(1) {
    
    for(i=160;i>0;i--) {
      servo2.write(i);
      
      OSSleep(servo_delay);
    }
    
    for(i=0;i<160;i++) {
      servo2.write(i);
      
      OSSleep(servo_delay);
    }
  } 
}

